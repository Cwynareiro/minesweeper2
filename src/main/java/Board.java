import java.util.ArrayList;
import java.util.List;

public class Board {



    private List<List<Space>> spaces= new ArrayList<List<Space>>();
    private int x;
    private int y;
    private int spacesWithBombNumber;
    private int notChosenSpaces;


    public int getNotChosenSpaces() {
        return notChosenSpaces;
    }

    public void setNotChosenSpaces(int notChosenSpaces) {
        this.notChosenSpaces = notChosenSpaces;
    }

    public Board(int x, int y, int spacesWithBombNumber) {

        this.x=x;
        this.y=y;
        this.spacesWithBombNumber=spacesWithBombNumber;
        this.notChosenSpaces=(x*y)-spacesWithBombNumber;
        for (int i=1; i<x +1; i++) {
            List<Space> col = new ArrayList<Space>();
            for (int j = 1; j < y +1; j++)
                col.add(new Space());
            spaces.add(col);
        }
    }





    public List<List<Space>> getSpaces() {
        return spaces;
    }

    public void setSpaces(List<List<Space>> spaces) {
        this.spaces = spaces;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }


    public int getSpacesWithBombNumber() {
        return spacesWithBombNumber;
    }

    public void setSpacesWithBombNumber(int spacesWithBombNumber) {
        this.spacesWithBombNumber = spacesWithBombNumber;
    }

    void ShowCurrentBoard(boolean access) {
        for (int i = 1; i < x + 1; i++)
            System.out.print("  " + i + "");
        System.out.println();
        if (access == true) {
            for (int i = 1; i < y + 1; i++) {
                System.out.print(i);
                for (int j = 1; j < x + 1; j++) {
                    if(spaces.get(j-1).get(i-1).isHasABomb()==true)
                    System.out.print("[#]");
                    else
                        if((spaces.get(j-1).get(i-1).isHasABomb()==false))
                    System.out.print("["+spaces.get(j-1).get(i-1).getBombsNearby()+"]");
                }
                System.out.println();
            }
        }
        else{
            for (int i = 1; i < y + 1; i++) {
                System.out.print(i);
                for (int j = 1; j < x + 1; j++) {
                    if(spaces.get(j-1).get(i-1).isHasABomb()==false && spaces.get(j-1).get(i-1).isWasChosen()==true)
                        System.out.print("["+spaces.get(j-1).get(i-1).getBombsNearby()+"]");
                    else
                        System.out.print("[ ]");
                }
                System.out.println();
            }
        }

    }



}
