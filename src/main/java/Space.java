public class Space {
    private boolean wasChosen;
    private boolean hasABomb;
    private int bombsNearby;

    @Override
    public String toString() {
        return "Space{" +
                "wasChosen=" + wasChosen +
                ", hasABomb=" + hasABomb +
                ", bombsNearby=" + bombsNearby +
                '}';
    }

    public Space() {
        this.wasChosen = false;
        this.hasABomb = false;
    }

    public boolean isWasChosen() {
        return wasChosen;
    }

    public void setWasChosen(boolean wasChosen) {
        this.wasChosen = wasChosen;
    }

    public boolean isHasABomb() {
        return hasABomb;
    }

    public void setHasABomb(boolean hasABomb) {
        this.hasABomb = hasABomb;
    }

    public int getBombsNearby() {
        return bombsNearby;
    }

    public void setBombsNearby(int bombsNearby) {
        this.bombsNearby = bombsNearby;
    }
}
