import java.util.Random;
import java.util.Scanner;

public class TheGame {

    private Board board;
    private int gameEnd = 0;

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    void gameStart() {
        Board plansza = new Board(9, 9, 10);
        setBoard(plansza);
        placeBombs();
        setBombCounters();
        while (gameEnd != 1) {
            board.ShowCurrentBoard(false);
            shootingPhase();

        }
        board.ShowCurrentBoard(true);
        if(board.getSpacesWithBombNumber()<=0) {
            System.out.println("You WON ");
        }
        else
            System.out.println("You LOST ");
    }


    void placeBombs() {
        int x;
        int y;
        Random generator = new Random();
        for (int i = 0; i < board.getSpacesWithBombNumber(); i++) {
            x = generator.nextInt(getBoard().getX());
            y = generator.nextInt(getBoard().getY());
            while (board.getSpaces().get(x).get(y).isHasABomb() == true) {
                x = generator.nextInt(getBoard().getX());
                y = generator.nextInt(getBoard().getY());
            }
            board.getSpaces().get(x).get(y).setHasABomb(true);
        }
    }

    void howManyAround(int x, int y) {
        int BombsAround = 0;
        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                if (i > -1 && i < board.getX() && j > -1 && j < board.getY()) {
                    if (board.getSpaces().get(i).get(j).isHasABomb() == true) {
                        BombsAround++;
                    }

                }
            }
            board.getSpaces().get(x).get(y).setBombsNearby(BombsAround);
        }

    }

    void setBombCounters() {
        for (int i = 0; i < board.getX(); i++) {
            for (int j = 0; j < board.getY(); j++) {
                howManyAround(i, j);
            }

        }
    }

    void shootingPhase() {
        Scanner spaceVariables = new Scanner(System.in);
        int x = spaceVariables.nextInt();
        int y = spaceVariables.nextInt();
        x = x - 1;
        y = y - 1;
        while(x>=board.getX() || y>=board.getY() || x<0 || y<0)
        {
            System.out.println("The space u tried to check does not exist, try again");
            x = spaceVariables.nextInt();
            y = spaceVariables.nextInt();
            x = x - 1;
            y = y - 1;
        }
        board.getSpaces().get(x).get(y).setWasChosen(true);
        board.setNotChosenSpaces(board.getNotChosenSpaces()-1);
        System.out.println("-1 ");
        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                if (i > -1 && i < board.getX() && j > -1 && j < board.getY()) {
                    if (board.getSpaces().get(i).get(j).getBombsNearby() == 0) {
                        for (int i2 = i - 1; i2 <= i + 1; i2++) {
                            for (int j2 = j - 1; j2 <= j + 1; j2++) {
                                if (i2 > -1 && i2 < board.getX() && j2 > -1 && j2 < board.getY()) {
                                    board.getSpaces().get(i2).get(j2).setWasChosen(true);
                                    if(board.getNotChosenSpaces()==0)
                                        gameEnd=1;
                                }
                            }

                        }
                    }
                }
                if (board.getSpaces().get(x).get(y).isHasABomb() == true)
                    gameEnd = 1;
            }
        }
    }
}

