## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [How to play](#how-to-play)

## General info
This project is a recreation of the game called Minesweeper known from Windows OS. I made it in order to practise algorithms and object-oriented programming.
	
## Technologies
Project is created with:
* Java
* Maven
	
## How to play
The game is played in your IDE's console and has the same rules as the original Minesweeper game. To try a space for a bomb you have to input two digits representing that space and click "Enter"
